<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\ThingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
//Auth
Route::get('/auth/registr', [AuthController::class, 'create']);
Route::post('/auth/registr', [AuthController::class, 'store']);
Route::get('/auth/login', [AuthController::class, 'login'])->name('login');
Route::post('/auth/login', [AuthController::class, 'customLogin']);
Route::get('/auth/logout', [AuthController::class, 'logout']);

//Place
Route::group(['prefix'=>'/place', 'middleware'=>'auth:sanctum'], function(){
    Route::get('/create', [PlaceController::class, 'create']);
    Route::post('/store', [PlaceController::class, 'store']);
    Route::get('/show/{id}', [PlaceController::class, 'show'])->name('show')->middleware('path');
    Route::get('/{id}/edit', [PlaceController::class, 'edit']);
    Route::put('/{id}', [PlaceController::class, 'update']);
    Route::get('/{id}/delete', [PlaceController::class, 'destroy']);
});

//Thing
Route::resource('thing', ThingController::class);
Route::get('/thing/{thing}/accept', [ThingController::class, 'accept']);
Route::get('/thing/{thing}/reject', [ThingController::class, 'reject']);


// Route::get('/', [MainController::class, 'index']);
Route::get('/', [PlaceController::class, 'index']);
Route::get('/galery/{full}', [MainController::class, 'show']);


Route::get('/about', function () {
    return view('main/about');
});

Route::get('/contact', function () {
    $contact = [
        'name' => 'Политех',
        'adres' => 'Пряники',
        'phone' => '8(495)423-2323',
        'email' => '@mospolytech.ru',
    ];
    return view('main/contact', ['contact' => $contact]);
});
