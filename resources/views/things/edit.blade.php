@extends('layouts.layout')
@section('content')
<form action="/thing/{{$thing->id}}" method="post">
    @csrf
    @method('PUT')
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Название</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name" value="{{$thing->name}}">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Описание</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="desc" value="{{$thing->desc}}">
  </div>
  <button type="submit" class="btn btn-primary">Изменить</button>
</form>
@endsection