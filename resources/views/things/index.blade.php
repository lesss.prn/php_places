@extends('layouts.layout')
@section('content')
<table class="table">
  <thead>
    <tr>
      <th scope="col">Place</th>
      <th scope="col">Name</th>
      <th scope="col">Desc</th>
    </tr>
  </thead>
  <tbody>
    @foreach($things as $thing)
    <tr>
      <td><a href="/place/show/{{$thing->place_id}}">{{App\Models\Place::where('id', $thing->place_id)->value('name')}}</a></td>
      <td>{{$thing->name}}</td>
      <td>{{$thing->desc}}</td>
      <td><a class="btn" href="/thing/{{$thing->id}}/accept">Принять</a>
          <a class="btn" href="/thing/{{$thing->id}}/reject">Отклонить</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$things->links()}}
@endsection