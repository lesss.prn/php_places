@extends('layouts.layout')
@section('content')
<table class="table">
  <thead>
    <tr>
      <th scope="col">Title</th>
      <th scope="col">Desc</th>
    </tr>
  </thead>
  <tbody>
    @foreach($places as $place)
    <tr>
      <td><a href="/place/show/{{$place->id}}">{{$place->name}}</a></td>
      <td>{{$place->desc}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$places->links()}}
@endsection