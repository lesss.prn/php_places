@extends('layouts.layout')
@section('content')

@if($errors->any())
    <div class="alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
        </ul>
    </div>
@endif

<form action="/place/{{$place->id}}" method="post">
    @method('PUT')
    @csrf
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Имя места</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name" value="{{$place->name}}">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Описание</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="description" value="{{$place->desc}}">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Тип места</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="repair" value="{{$place->repair}}">
  </div>
  <button type="submit" class="btn btn-primary">Изменить</button>
</form>
@endsection