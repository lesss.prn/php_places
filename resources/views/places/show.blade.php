@extends('layouts.layout')
@section('content')
<div class="card" style="margin-top">
  <div class="card-body">
    <h5 class="card-title">{{$place->name}}</h5>
    <h6 class="card-subtitle mb-2 text-muted">{{$place->desc}}</h6>
    <p class="card-text">{{$place->desc}}</p>
    <a href="/place/{{$place->id}}/edit" class="btn btn-info">Редактирование</a>
    <a href="/place/{{$place->id}}/delete" class="btn btn-warning">Удаление</a>
  </div>

  <h3 class="text-center">Вещи</h3>
  @isset($_GET['result'])
    @if($_GET['result'])
      <div class="alert alert-primary">
        Ваша вещь отправлена на модерацию!
      </div>
    @endif
  @endisset

  @foreach($things as $thing)
  <form action="/thing/{{$thing->id}}" method="post">
    @csrf
    @method('DELETE')
    <div class="card-body">
    <h5 class="card-title">{{$thing->name}} </h5>
    <p class="card-text">{{$thing->desc}}</p>
    @can('update-thing', $thing)
      <a href="/thing/{{$thing->id}}/edit" class="btn btn-secondary">Редактирование</a>
      <button type="submit" class="btn btn-secondary">Удалить</button>
    @endcan
  </div>
  </form>
  @endforeach
  {{$things->links()}}
</div>

@if($errors->any())
    <div class="alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
        </ul>
    </div>
@endif

<form action="/thing" method="post">
    @csrf
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Имя</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Описание</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="desc">
  </div>
  <input type="hidden" name="id" value="{{$place->id}}">
  <button type="submit" class="btn btn-primary">Добавить новую вещь</button>
</form>
@endsection