<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StatMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $articleCount;
    protected $commentCount;

    public function __construct($placeCount, $thingCount)
    {
        $this->placeCount = $placeCount;
        $this->thingCount = $thingCount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('moosbeere_O@mail.ru')
                    ->to('moosbeere_O@mail.ru')
                    ->with(['placeCount' => $this->placeCount, 'thingCount' => $this->thingCount])
                    ->view('mail.stat');
    }
}
