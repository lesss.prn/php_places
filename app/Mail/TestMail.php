<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Place;
use App\Models\Thing;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $place;
    protected $thing;

    public function __construct(Place $place, Thing $thing)
    {
        $this->place = $place;
        $this->thing = $thing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('moosbeere_O@mail.ru')
                    ->to('moosbeere_O@mail.ru')
                    ->with(['place' => $this->place, 'thing' => $this->thing])
                    ->view('mail.send');
    }
}
