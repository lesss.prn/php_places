<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Place;
use App\Models\Thing;
use App\Events\NewArticleEvent;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class PlaceController extends Controller
{
    public function index(){
        $currentPage = request('page');
        $places = Cache::remember('places:all1'.$currentPage, 2000, function(){
            return Place::latest()->paginate(5);
        }); 
        return view('places/index', ['places' => $places]);
    }

    public function create(){
        $this->authorize('create', [self::class]);
        return view('places/create');
    }
    
    public function store(Request $request){
        $this->authorize('create', [self::class]);

        $caches = DB::table('cache')->whereRaw('`key` GLOB :name', ['name'=>'places:all*[0-9]'])->get();
        foreach($caches as $cache){
            // Log::alert($cache->key);
            Cache::forget($cache->key);
        }
        
        $request->validate([
            'name' => 'required',
            'desc' =>'required|min:10',
            'repair' =>'required',
        ]);
        $place = new Place();
        $place->name = request('name');
        $place->desc = request('desc');
        $place->repair = request('repair');
        // $place->work = request('work');
        $place->save();
        $users = User::where('id', '!=', auth()->id())->get();
        Cache::flush();
        return redirect('/');
    }

    public function show($id){
        $array = Cache::rememberForever('place/show/'.$id, function()use($id){
            $places = Place::FindOrFail($id);
            $things = Thing::where([
                            ['place_id', $id],
                            ['accept', 1]
                        ])->latest()->paginate(5);
            return ['place'=>$places, 'things'=>$things];
        });
        // return view('articles.show', ['article' => $array['articles'], 'comments'=>$array['comments']]);

        return view('places.show', $array);
        
    }

    public function edit($id){
        $places=Place::FindOrFail($id);
        $this->authorize('update', [self::class, $places]);
        return view('places.edit', ['place'=>$place]);
    }

    public function update(Request $request, $id){
        
        $request->validate([
            'name' => 'required',
            'description' =>'required|min:10',
            'repair' =>'required',
        ]);
        Cache::flush();
        $place=Place::FindOrFail($id);
        $this->authorize('update', [self::class, $place]);
        $place->name = request('name');
        $place->desc = request('description');
        $place->repair = request('repair');
        $place->work = request('work');
        $place->save();
        return redirect()->route('show', ['id'=>$place->id]);
    }

    public function destroy($id){
        Cache::flush();
        $place=Place::FindOrFail($id);
        $this->authorize('delete', [self::class, $place]);
        Thing::where('place_id', $id)->delete();
        $place->delete();
        return redirect('/');
    }
}

