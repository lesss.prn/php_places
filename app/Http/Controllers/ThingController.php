<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thing;
use Illuminate\Support\Facades\Gate;
use App\Models\Place;
use App\Jobs\VeryLongJob;
use Illuminate\Support\Facades\Cache;

class ThingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $things = Thing::where('accept', null)->latest()->paginate(10);
        return view('things.index', ['things'=>$things]);
    }

    public function accept(Thing $thing){
        $thing->accept = 1;
        $thing->save();
        Cache::flush();
        return redirect()->back();
    }

    public function reject(Thing $thing){
        $thing->accept = 0;
        $thing->save();
        Cache::flush();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required',
        ]);
        $thing = new Thing();
        $thing->name = request('name');
        $thing->desc = request('desc');
        $thing->place()->associate(request('id'));
        $thing->user()->associate(auth()->user());
        $result = $thing->save();
        $place = Place::where('id', $thing->place_id)->first();
        // if ($request){
        //     VeryLongJob::dispatch($place, $thing);
        // }
        Cache::flush();
        return redirect()->route('show', ['id'=>request('id'), 'result'=>$result]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thing = Thing::FindOrFail($id);
        Gate::authorize('update-thing', $thing);
        return view('things.edit', ['thing' => $thing]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required',
        ]);
        $thing = Thing::FindOrFail($id);
        Gate::authorize('update-thing', $thing);
        $thing->name = request('name');
        $thing->desc = request('desc');
        $thing->save();
        Cache::flush();
        return redirect()->route('show', ['id' => $thing->place_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thing = Thing::FindOrFail($id);
        Gate::authorize('update-thing', $thing);
        $thing->delete();
        Cache::flush();
        return redirect()->route('show', ['id' => $thing->place_id]);
    }
}
