<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Thing;




class Place extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'desc',
        'repair',
        'work',
    ];

    public $timestamps = false;
    
    public function things(){
        return $this->hasMany(Thing::class);
    }
}
