<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Place;

class Thing extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'desc',
        'wrnt',
    ];

    public $timestamps = false;
    
    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'master_id');
    }
}

